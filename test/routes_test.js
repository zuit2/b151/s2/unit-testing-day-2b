const chai = require("chai");
// const expect = chai.expect;
const { expect } = require("chai");
const http = require("chai-http");

chai.use(http);

const url = "http://localhost:5001";

describe("API Test Suite", () => {
  it("Test API Get people is running", () => {
    chai
      .request(url)
      .get("/people")
      .end((err, res) => {
        expect(res).to.not.equal(undefined);
      });
  });
  it("Test API Get people returns 200", () => {
    chai
      .request(url)
      .get("/people")
      .end((err, res) => {
        expect(res.status).to.equal(200);
      });
  });
  it("Test API post person returns 400 if no NAME property", (done) => {
    chai
      .request(url)
      .post("/person")
      .type("json")
      .send({
        alias: "Ackerman",
        age: 27,
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });
  it("Test API Post is running", () => {
    chai
      .request(url)
      .post("/")
      .end((err, res) => {
        expect(res).to.not.equal(undefined);
      });
  });
  it("Test API post person returns error if no alias", (done) => {
    chai
      .request(url)
      .post("/person")
      .type("json")
      .send({
        name: "Mikasa",
        age: 27,
      })
      .end((err, res) => {
        console.log(res.status);
        expect(res.status).to.equal(400);
        done();
      });
  });
  it("Test API post person returns error if no AGE property", (done) => {
    chai
      .request(url)
      .post("/person")
      .type("json")
      .send({
        alias: "Ackerman",
        name: "Mikasa",
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });
});
