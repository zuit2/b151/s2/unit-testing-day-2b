// create a factorial function

const factorial = (n) => {
  if (typeof n !== "number") return undefined;
  if (n < 0) return undefined;
  if (n === 0) return 1;
  if (n === 1) return 1;
  return n * factorial(n - 1);
};

const div_check = (n) => {
  // console.log(n%d);
  let div5 = false;
  let div7 = false;

  // console.log(n % 5);
  // console.log(n % 7);

  n % 5 === 0 ? (div5 = true) : (div5 = false);
  n % 7 === 0 ? (div7 = true) : (div7 = false);

  if (div5 && !div7) {
    return "Divisible by 5 only";
  } else if (div7 && !div5) {
    return "Divisible by 7 only";
  } else if (div5 && div7) {
    return "Divisible by 5 and 7";
  } else {
    return "Not divisible by 5 or 7";
  }
};

const names = {
  Eren: {
    alias: "Attack Titan",
    name: "Eren Yeager",
    age: 28,
  },
  Levi: {
    alias: "Clean Boi",
    name: "Levi Ackerman",
    age: 38,
  },
};

module.exports = {
  factorial: factorial,
  div_check: div_check,
  names: names,
};
